import argparse
import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '../'))

import cv2

from vedastr.runners import InferenceRunner
from vedastr.utils import Config

import csv

def parse_args():
    parser = argparse.ArgumentParser(description='Inference')
    parser.add_argument('config', type=str, help='Config file path')
    parser.add_argument('checkpoint', type=str, help='Checkpoint file path')
    parser.add_argument('image', type=str, help='input image path')
    parser.add_argument('gpus', type=str, help='target gpus')
    args = parser.parse_args()

    return args


def main():
    args = parse_args()

    cfg_path = args.config
    cfg = Config.fromfile(cfg_path)

    deploy_cfg = cfg['deploy']
    common_cfg = cfg.get('common')
    deploy_cfg['gpu_id'] = args.gpus.replace(" ", "")

    runner = InferenceRunner(deploy_cfg, common_cfg)
    runner.load_checkpoint(args.checkpoint)
    if os.path.isfile(args.image):
        images = [args.image]
    else:
        images = [os.path.join(args.image, name)
                  for name in os.listdir(args.image)]

    if not os.path.exists('fa'):
        os.makedirs('fa')


    with open('submission.csv', 'w', newline='') as outcsv:
        writer = csv.writer(outcsv)
        writer.writerow(["id", "text", "conf"])
    
        image_counts = 0
        image_correct_counts = 0
        for img in images:
            filename = os.path.basename(img)
            image_counts = image_counts + 1
            image = cv2.imread(img)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            pred_str, probs = runner(image)
            writer.writerow([os.path.splitext(filename)[0], pred_str[0].upper(), str(float(probs[0]))])
            gt = os.path.splitext(img)[0].split('_')[-1]
            if pred_str[0].upper() == gt:
                image_correct_counts = image_correct_counts + 1
            else:
                print(img)
                runner.logger.info('pred_str:' + pred_str[0].upper())
                runner.logger.info('gt:' + gt)
                cv2.imwrite(os.path.join('fa', os.path.splitext(filename)[0] + '_' + pred_str[0].upper() + '_' + str(float(probs[0])) + os.path.splitext(filename)[1]), image)
            #runner.logger.info('predict string: {} \t of {}'.format(pred_str, img))
        runner.logger.info('acc:' + str(image_correct_counts) + '/' + str(image_counts) + '=' + str(image_correct_counts/image_counts))


if __name__ == '__main__':
    main()
